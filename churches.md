Hello,

Pornography is a massive problem everywhere, and you no doubt have many people seeking help from you and living with shame.

A lot of people wrestle with quitting pornography because they try using willpower -- anything involving counting days, blocking, restricting, rewarding or punishing yourself for using porn -- but these approaches don't work because they don't target the reasons that people have for using pornography in the first place. 

Porn users must believe that pornography has benefits, such as stress relief, aiding concentration or relieving boredom, or helping with any other reason they could think of, otherwise they just wouldn't use it. Though, since porn is something that we can't chemically handle, it creates a tiny little withdrawal in the brain, leaving people *more* stressed, less relaxed, and less able to concentrate than someone who never started using it.

It's this tug of war that makes all addictions difficult to stop: "it's not good for me, but I enjoy it", and porn is sneakily sad due it to being introduced at such a young age.
By showing users that their reasons for pornography use are illusions, people quit painlessly because they're no longer wrestling with themselves. They notice a thought that they want to use pornography, and they realise that it's a lie from a 'little monster' inside their brain craving to be fed.



As pornography is very easy to quit when done correctly, all you need to do to cure the world of it just is show people this illusion, and then teach them how to quit (by recognising they don't enjoy it, and genuinely enjoy being free) and they'll just naturally spread the message forward to others.

You're receiving this letter because there's a book that has helped many people that does just that, and it's completely free and always will be, and someone who read it wanted to share this letter to someone in their local community.

I think that the only thing holding good discussions back is the stigma -- partly out of being labeled as an 'addict' (instead of a porn 'user') and the uncomfortable nature of recognising the problem.
Fortunately, the tide is shifting against pornography -- scientifically, socially and spiritally -- and we just need to unite people together.


I'm the author of easypeasymethod.org, which has helped millions of people to quit pornography. People regard it as 'the method to quit', and it's grown to this size through word of mouth because it works.

My purpose is creating mass social change against pornography through changing the narrative around it and bringing people together instead. Hence, the book is and always will be completely free, and my aim is making it as accessible as possible, everywhere.

As such, we're asking for your help.

All the people in your congregation, even if they don't use it themselves, are directly and indirectly impacted by pornography -- and freeing themselves, and those they love, would mean the message growing very quickly.


If you'd like to help freeing people from pornography, I'd ask that you consider how to reach different people within your congregation. For young people, there's group chats and men's groups (and women!) and we've found that discussing lust during a sermon works very well too.

You're also welcome pointing people to easypeasymethod.org and quiteasily.org, which also have guides for creating online and local impact against pornography. You're reaading this letter because of those.

I'd also be very grateful if you considered passing this letter to anyone you believe would find it interesting.


You might find that discussing a normalised addiction means it's no longer talked about in hushed tones, with people who were silently struggling getting the help they need and creating more harmonious families, communities and societies.

If you're open to discussing it with your congregation, you may need to warn people that you'll be discussing a sensitive topic next week, and providing an alternative sermon for young children.

You may need to coach concerned parents on how to discuss porn without stigmatising -- recognising that many, many young men (and women) use pornography, and most will deny it, so simply mentioning resources and giving a safe place to seek help works best.


easypeasy and quiteasily also offboard people into creating change locally, with letters for schools and subtle posters for communities; alongside socially, through encouraging people to #shareyourstory about how pornography has impacted everyone.

Ultimately, with media organisations addressing ubiquitous pornography consumption and it's impacts more openly, and politicians ensuring that young children aren't exposed, and adolescents are educated, we can begin creating a more beautiful world.


Pornography affects billions, regardless of demographic or belief, so a world where nobody wants to be used by it is a genuinely utopian one; so unless we're willing to blush a little bit now and have honest conversations about it, the porn industry will succeed in making virtual reality the 'next big thing'.

We're unprepared now, and I really shudder thinking of that.


Fortunately, I guarantee that after discussing pornography more openly you'll see positive impacts immediately, as your congregation frees themselves and are inspired to help others.

I think this paves the way for more honest discussions on a wide range of issues, and more ideas for solving them.

After all, if billions of people were suddenly freed from lust, what else could we focus on instead?

Yours,
Fraser Patterson
peacefulfoundation.org
